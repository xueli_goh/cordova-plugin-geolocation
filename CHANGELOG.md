# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.0.1-OS2]

### Additions

- Add new native implementation on Android [RNMT-2811](https://outsystemsrd.atlassian.net/browse/RNMT-2811)

## [4.0.1-OS1]

### Additions
- Adds NSLocationAlwaysUsageDescription preference to plugin.xml [RNMT-2651](https://outsystemsrd.atlassian.net/browse/RNMT-2651)

## [4.0.1-OS]
- Fix to allow 4.0.0 version install [CB-13705](https://issues.apache.org/jira/browse/CB-13705)

[Unreleased]: https://github.com/OutSystems/cordova-plugin-geolocation/compare/4.0.1-OS...HEAD
[4.0.1-OS2]: https://github.com/OutSystems/cordova-plugin-geolocation/compare/4.0.1-OS1...4.0.1-OS2
[4.0.1-OS1]: https://github.com/OutSystems/cordova-plugin-geolocation/compare/4.0.1-OS...4.0.1-OS1
[4.0.1-OS]: https://github.com/OutSystems/cordova-plugin-geolocation/compare/4.0.1...4.0.1-OS